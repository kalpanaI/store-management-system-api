
**Store Management System**

**Table of contents**

- Title
- Description
- Tools and technologies
- Features
- Contact


**Title**
Store Management System : The application is it consists of different stocks where the stocks are added by an admin and users can buy them from their choice.

**Description**
Store Management System : The application is it consists of different stocks where the stocks are added by an admin and users can buy them from their choice.Here we have two modules they are

1. 	Admin service
2. 	User service



**Admin service:**
The admin will be able to control login IDs and access. Admin will get login with a valid username and password. The admin has access to the stock management information system. He can add, update and delete the stock.Admin can add the details of the supply forms. Admin views all the user's information. 

**User service:**
User needs to fill the registration and get a valid username and password and then user will be able to check the stock availability and buy the stock. 

**Tools and technologies:**

-   Java 8
- 	Spring Boot
- 	Spring security
- 	Spring Web MVC
- 	Spring Data JPA
- 	Hibernate
- 	MySQL Database
- 	Swagger
- 	Postman
- 	Maven

	

**Features**

**Administrator**

- 	Stock Management
- 	User management
- 	Order management


**User**

- 	Registration
- 	Stock details
- 	Order details



**Contact**

**Email**: kalpana.ithagoni@wavelabs.ai, keerthana.bathula@wavelabs.ai
