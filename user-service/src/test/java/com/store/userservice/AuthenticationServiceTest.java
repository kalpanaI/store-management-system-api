package com.store.userservice;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.store.userservice.enums.Role;
import com.store.userservice.model.AuthenticationToken;
import com.store.userservice.model.User;
import com.store.userservice.repository.TokenRepository;
import com.store.userservice.service.AuthenticationService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class AuthenticationServiceTest {
	@MockBean
	AuthenticationToken authenticationToken;
	
	@MockBean
	TokenRepository repository;
	@Autowired
	AuthenticationService authenticationservice;
	
	String token="d4a2808b-b545-49ab-b9f3-f709b93c49ca";
	User user=new User("kalpana","ithagoni","kalpanaithagoni@gmail.com","KalpanaI","99887788777",Role.USER,"Kalpana@3");
	
	 @Test
	 @Order(1)
	 public void testsaveConfirmationToken() {
		 AuthenticationToken authenticate=new AuthenticationToken();
					when(repository.save(Mockito.any())).thenReturn(authenticate);
					assertEquals(authenticate,authenticationservice.saveConfirmationToken(authenticate));
	}
	@Test
	@Order(2)
	public void testgetUser() {
		AuthenticationToken authenticate=new AuthenticationToken();
		when(repository.findTokenByToken(Mockito.any())).thenReturn(authenticate);
		when(authenticationToken.getUser()).thenReturn(user);
		assertNotEquals(authenticate,authenticationservice.getUser(token));
		
		}
	
	@Test
	@Order(3)
	public void testgetToken() {
		AuthenticationToken authenticate=new AuthenticationToken();
		when(repository.findTokenByUser(user)).thenReturn(authenticate);
		assertEquals(authenticate,authenticationservice.getToken(user));
	}
 
	
}
