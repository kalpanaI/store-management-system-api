package com.store.userservice;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.store.userservice.model.Stock;
import com.store.userservice.repository.StockRepository;
import com.store.userservice.service.StockService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class StockServiceTest {
	@MockBean
	StockRepository stockRepository;
	
	@Autowired
	StockService stockService;
		
	@Test
	@Order(1)
	public void testReadAllStock () {
		List<Stock> stocks =new ArrayList<Stock>();
		stocks.add(new Stock(1,"Book",500.00,'A',20));
		stocks.add(new Stock(2,"Bags",600.00,'A',10));
		when(stockRepository.findAll()).thenReturn(stocks);//Mocking
		assertEquals(2,stockService.listStocks().size());
	}	
	@Test
	@Order(2)
	public void testgetStockByName() {
		List<Stock> stock=new ArrayList<Stock>();
		stock.add(new Stock(1,"Book",500.00,'A',20));
		stock.add(new Stock(2,"Bags",600.00,'A',10));
		when(stockRepository.findByName("Book")).thenReturn(stock);
		assertEquals(stock,stockService.getStockByName("Book"));
	}
	@Test
	@Order(4)
	public void testgetStockById() {		
		Stock stock=new Stock(1,"Book",500.00,'A',20);
    	when(stockRepository.findById(1)).thenReturn(Optional.of(stock));
		assertTrue(stock.equals(stockService.getStockById(1)));
	}
}
