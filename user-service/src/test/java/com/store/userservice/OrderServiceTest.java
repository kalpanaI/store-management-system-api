package com.store.userservice;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.store.userservice.enums.Role;
import com.store.userservice.model.Orders;
import com.store.userservice.model.Stock;
import com.store.userservice.model.User;
import com.store.userservice.repository.OrderRepository;
import com.store.userservice.repository.StockRepository;
import com.store.userservice.service.OrderService;
import com.store.userservice.service.StockService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class OrderServiceTest {
	
	@Mock
	OrderRepository orderRepository=mock(OrderRepository.class);
	@Mock
	StockRepository stockRepository=mock(StockRepository.class);
	@Mock
	StockService stockService=mock(StockService.class);
	@InjectMocks
	OrderService orderService;
	User user = new User("kalpana","ithagoni","kalpanaithagoni@gmail.com","KalpanaI","99887788777",Role.USER,"Kalpana@3");
	Stock stock=new Stock(1,"Books",250.00d,'A',3);	
	Orders orders=new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock);
	
	@Test
	@Order(1)
	public void testPlaceOrder() {
		Orders order=new Orders();
		order.setId(1);
		order.setOrdereditem("Books");
		order.setOrderdate(LocalDate.now());
		order.setAddress("Nalgonda");
		order.setStock(stock);
		order.setUser(user);
		order.setQuantity(2);
		order.setTotalPrice(500.00d);
		order.setTypeOfTransaction("COD");
		when(stockService.getStockById(1)).thenReturn(stock);
		when(orderRepository.save(order)).thenReturn(order);
		assertEquals(true,orderService.updateStock(1, 2));
		assertNotEquals(order,orderService.placeOrder("Nalgonda", user, 1, 2));
		
	}

	@Test
	@Order(2)
	public void viewOrderListtest() {
		List<Orders> orders = orderRepository.findAll();
		User user=new User();
		orders.add(new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock));
		when(orderRepository.findAll()).thenReturn(orders);			
		assertEquals(orders.size(),orderService.viewOrderList(user).size());
	}
	
    @Test
	@Order(3)
	public void testgetOrderById() {		
    	when(orderRepository.findById(1)).thenReturn(Optional.of(orders));
		assertTrue(orders.equals(orderService.getOrderById(1)));
	}

	@Test
	@Order(4)
	public void testupdateStock() {
		when(stockService.getStockById(1)).thenReturn(stock);
		when(stockRepository.save(Mockito.any())).thenReturn(stock);
		assertNotEquals(orders,orderService.updateStock(1, 2));
	}
	
}
