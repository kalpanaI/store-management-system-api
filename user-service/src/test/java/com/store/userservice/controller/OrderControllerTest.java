package com.store.userservice.controller;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.store.userservice.config.ApiResponse;
import com.store.userservice.enums.Role;
import com.store.userservice.model.Orders;
import com.store.userservice.model.Stock;
import com.store.userservice.model.User;
import com.store.userservice.repository.OrderRepository;
import com.store.userservice.repository.StockRepository;
import com.store.userservice.service.AuthenticationService;
import com.store.userservice.service.OrderService;
import com.store.userservice.service.StockService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class OrderControllerTest {
	@MockBean
	OrderService orderService;
	@MockBean
	OrderRepository orderRepository;
	@MockBean
	AuthenticationService authenticationService;
	@MockBean
	StockService stockService;
	@MockBean
	StockRepository stockRepository;
	@Autowired
	OrderController ordercontroller;
	Stock stock=new Stock(1,"Books",800.00,'A',20);
	User user = new User("kalpana","ithagoni","kalpanaithagoni@gmail.com","KalpanaI","99887788777",Role.USER,"Kalpana@3");
	Orders order = new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock);
	
	String token="89iujhghjnbbvcgh";
	List<Orders> orders;
	Orders Order;
	@Test
	@Order(1)
	public void testPlaceOrder(){
		order = new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock);
		when(stockService.getStockById(1)).thenReturn(stock); //Mocking
		when(orderService.placeOrder("Nalgonda", user,1, 2)).thenReturn(order); //Mocking
		when(orderRepository.save(order)).thenReturn(Order);
		ResponseEntity<ApiResponse> res=ordercontroller.placeOrder("Nalgonda", 1, 2, token);
		assertNotEquals(true,res.getBody().isSuccess());
		assertEquals("stock unavailable",res.getBody().getMessage());		
	//	assertEquals("Order has been placed",res.getBody().getMessage());
		assertEquals(LocalDateTime.now().toString(),res.getBody().getTimestamp());
		
	}
	@Test
	@Order(2)
	public void testviewOrderList(){
		orders=new ArrayList<Orders>();
		orders.add(new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock));
		orders.add(new Orders(2,2,"Books",1500.00d,LocalDate.now(),"Hyderabad","COD",user,stock));
		authenticationService.authenticate(token);
		when(authenticationService.getUser(token)).thenReturn(user); //Mocking		
		when(orderService.viewOrderList(user)).thenReturn(orders); //Mocking
		ResponseEntity<List<Orders>> res=ordercontroller.viewOrderList(token);
		assertNotEquals(HttpStatus.FOUND,res.getStatusCode());
		assertEquals(2,res.getBody().size());

 }
	@Test
	@Order(3)
	public void testgetOrderById() {
		Order=new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock);
		when(orderService.getOrderById(1)).thenReturn(Order);	//Mocking
		ResponseEntity<Object> res=ordercontroller.getOrderById(1, token);
		assertEquals(HttpStatus.OK,res.getStatusCode());
		assertNotEquals(HttpStatus.NOT_FOUND,res.getStatusCode());
	}
	
}