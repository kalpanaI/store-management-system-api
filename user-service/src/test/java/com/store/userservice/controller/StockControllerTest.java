package com.store.userservice.controller;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.store.userservice.enums.Role;
import com.store.userservice.model.Stock;
import com.store.userservice.model.User;
import com.store.userservice.service.AuthenticationService;
import com.store.userservice.service.StockService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
public class StockControllerTest {
	@MockBean
	AuthenticationService authenticationService;
	@MockBean
	StockService stockService;
	@Autowired
	StockController stockController;
	User user = new User("kalpana","ithagoni","kalpanaithagoni@gmail.com","KalpanaI","99887788777",Role.USER,"Kalpana@3");
	List<Stock> stocks;
	Stock stock;
	String token=null;
	@Test
	@Order(1)
	public void testgetStocks() {
		stocks=new ArrayList<Stock>();
		stocks.add(new Stock(1,"Books",800.00,'A',20));
		stocks.add(new Stock(2,"Bags",1200.00,'A',10));
		when(authenticationService.getUser(token)).thenReturn(user); 
		when(stockService.listStocks()).thenReturn(stocks); 
		ResponseEntity<List<Stock>> res=stockController.getStocks(token);
		assertEquals(HttpStatus.OK,res.getStatusCode());
		assertEquals(2,res.getBody().size());
	}
	@Test
	@Order(2)
	public void testgetStockByName() {
		stocks=new ArrayList<Stock>();
		stocks.add(new Stock(1,"Books",800.00,'A',20));
		stocks.add(new Stock(2,"Bags",1200.00,'A',10));
		when(stockService.getStockByName("Books")).thenReturn(stocks);	//Mocking
		ResponseEntity<List<Stock>> res=stockController.getStockByName("Books");
		assertEquals(HttpStatus.OK,res.getStatusCode());
		assertEquals("Books",res.getBody().get(0).getName());
	}
	@Test
	@Order(5)
	public void testgetStockById() {
		stock=new Stock(1,"Books",800.00,'A',20);
		when(stockService.getStockById(1)).thenReturn(stock);	//Mocking
		ResponseEntity<Object> res=stockController.getStockById(1, token);
		assertEquals(HttpStatus.OK,res.getStatusCode());
	}
}
