package com.store.userservice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.store.userservice.dto.ResponseDto;
import com.store.userservice.dto.SignInDto;
import com.store.userservice.dto.SignInResponseDto;
import com.store.userservice.dto.SignupDto;
import com.store.userservice.enums.Role;
import com.store.userservice.model.AuthenticationToken;
import com.store.userservice.model.User;
import com.store.userservice.repository.UserRepository;
import com.store.userservice.service.AuthenticationService;
import com.store.userservice.service.UserService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class UserControllerTest {
		@MockBean
		UserService userService;
		@MockBean
		UserRepository userRepository;
		@MockBean
		AuthenticationService authenticationservice;
		@MockBean
		AuthenticationToken authenticationToken;
		@Autowired
		UserController usercontroller;
		User user = new User("kalpana","ithagoni","kalpanaithagoni@gmail.com","KalpanaI","99887788777",Role.USER,"Kalpana@3");
		List<User> users=new ArrayList();
		User User;
		String token="98tuygffcghvjh";
		
		@Test
		@Order(1)
		public void testsignup() {
			SignupDto SignupDto=new SignupDto("kalpana","ithagoni","kalpanaithagoni@gmail.com","99887788777","Kalpana1","Kalpana@3");
			ResponseDto Responsedto=new ResponseDto("success","user created successfully");
			when(userService.signUp(Mockito.any())).thenReturn(Responsedto);
			when(authenticationservice.getToken(User)).thenReturn(authenticationToken);
			assertEquals("success",usercontroller.Signup(SignupDto).getStatus());
			assertEquals("user created successfully",usercontroller.Signup(SignupDto).getMessage());
		}
		@Test
		@Order(2)
		public void testsignIn() {
			SignInDto SignInDto=new SignInDto("kalpanaI","Ithagoni@3");
			SignInResponseDto ResponseDto=new SignInResponseDto("success","d4a2808b-b545-49ab-b9f3-f709b93c49ca");
			when(userService.signIn(Mockito.any())).thenReturn(ResponseDto);
			assertEquals("success",usercontroller.SignIn(SignInDto).getStatus());
			assertEquals("d4a2808b-b545-49ab-b9f3-f709b93c49ca",usercontroller.SignIn(SignInDto).getToken());
			
		}
		@Test
		@Order(3)
		public void testresetPassword() throws NoSuchAlgorithmException {
			String username = "kalpanaI";
			String password = "Ithagoni@4";
			String confirmPassword = "Ithagoni@4";
			when(userService.resetPassword(username, password, confirmPassword)).thenReturn(confirmPassword);
			ResponseEntity<String> res=usercontroller.resetPassword(username, password, confirmPassword);		
			assertEquals(HttpStatus.OK,res.getStatusCode());
			assertNotEquals(HttpStatus.NOT_FOUND,res.getStatusCode());
		}
		
	}
