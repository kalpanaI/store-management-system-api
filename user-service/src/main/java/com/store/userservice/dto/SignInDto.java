package com.store.userservice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import com.store.userservice.validation.ValidPassword;
@Validated
public class SignInDto {


	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your name")
    private String username;
    	
	@Length(min = 8, message = "*Your password must have at least 8 characters")
    @NotEmpty(message = "*Please provide your password")
	@ValidPassword
    private String password;
    

	public SignInDto(@Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your name") String username,
			@Length(min = 8, message = "*Your password must have at least 8 characters") @NotEmpty(message = "*Please provide your password") String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	
}
