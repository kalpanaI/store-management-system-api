package com.store.userservice.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "stock")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "Stock_Name ")
    private @NotNull String name;
    @Column(name = "Price_Per_Unit ")
    private @NotNull double price;
    @Column(name = "Quality")
    private @NotNull char quality;
    @Column(name = "Availability ")
    private @NotNull int availability;
    
    @JsonIgnore
    @OneToMany(mappedBy = "stock",
            fetch = FetchType.LAZY)
    private List<Orders> orders;
    public Stock(Integer id,String name, double price, char quality,int availability) {
        super();
        this.id=id;
        this.name = name;
        this.price = price;
        this.quality = quality;
        this.availability=availability;
    }
    public Stock() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public char getQuality() {
        return quality;
    }

    public void setQuality(char quality) {
        this.quality = quality;
    }

   

    public int getAvailability() {
		return availability;
	}

	public void setAvailability(int availability) {
		this.availability = availability;
	}

	@Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quality='" + quality + '\'' +
                ", availability='" + availability + '\'' +
                '}';
    }
	public List<Orders> getOrders() {
		return orders;
	}
	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}

}
