package com.store.userservice.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "orders")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Orders {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "quantity ")
	private int quantity ;
	
	@Column(name = "ordereditem ")
	private String ordereditem ;
	
	@Column(name = "totalprice ")
	private double totalPrice ;
	@Column(name = "orderdate  ")
	private LocalDate orderdate  ;

	@Column(name = "address ")
	private String Address ;
	
	@Column(name = "type_transaction")
	private String typeOfTransaction;
    
	@ManyToOne()
    @JsonIgnore
    @JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@ManyToOne()
    @JsonIgnore
    @JoinColumn(name = "stock_id", referencedColumnName = "id")
	private Stock stock;
	public Orders() {
		
	}	
	
	public Orders(int id, int quantity, String ordereditem, double totalPrice, LocalDate orderdate, String address,
			String typeOfTransaction, User user, Stock stock) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.ordereditem = ordereditem;
		this.totalPrice = totalPrice;
		this.orderdate = orderdate;
		this.Address = address;
		this.typeOfTransaction = typeOfTransaction;
		this.user = user;
		this.stock = stock;
	}
	@Override
	public String toString() {
		return "Order [id=" + id + ", quantity=" + quantity + ", ordereditem=" + ordereditem + ", totalPrice="
				+ totalPrice + ", orderdate=" + orderdate + ", Address=" + Address + ", typeOfTransaction="
				+ typeOfTransaction + ", user=" + user + ", stock=" + stock + "]";
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public LocalDate getOrderdate() {
		return orderdate;
	}


	public void setOrderdate(LocalDate orderdate) {
		this.orderdate = orderdate;
	}


	public String getAddress() {
		return Address;
	}


	public void setAddress(String address) {
		Address = address;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public double getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}


	public User getUser() {
		return user;
	}

    public void setUser(User user) {
		this.user = user;
	}
	public String getTypeOfTransaction() {
		return typeOfTransaction;
	}
	public void setTypeOfTransaction(String typeOfTransaction) {
		this.typeOfTransaction = typeOfTransaction;
	}


	public String getOrdereditem() {
		return ordereditem;
	}


	public void setOrdereditem(String ordereditem) {
		this.ordereditem = ordereditem;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
}