package com.store.userservice.enums;

public enum ResponseStatus
{
    SUCCESS,
    ERROR
}
