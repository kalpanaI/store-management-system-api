package com.store.userservice.enums;

public enum Role {
    USER,
    ADMIN
}
