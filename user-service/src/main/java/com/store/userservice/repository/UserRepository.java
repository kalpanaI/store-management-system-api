package com.store.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.store.userservice.model.User;
import java.util.List;
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findAll();

    User findByUsername(String username);

    User findUserByUsername(String username);


	@Query("SELECT u FROM User u WHERE u.email = ?1")
    public User findByEmail(String email); 

}
