package com.store.userservice.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.userservice.model.Orders;
import com.store.userservice.model.User;
@Repository
public interface OrderRepository extends JpaRepository<Orders, Integer>{

	boolean existsById(int orderid);

	Orders getOne(int orderid);

	List<Orders> findAllByUserOrderByOrderdateDesc(User user);
	
}
