package com.store.userservice.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.store.userservice.model.Stock;
import com.store.userservice.model.User;
@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {

	Stock findById(User user);

	Stock getStockById(int id);

	public java.util.List<Stock> findByName(String name);
}
