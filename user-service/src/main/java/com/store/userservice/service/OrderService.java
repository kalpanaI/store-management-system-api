package com.store.userservice.service;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.store.userservice.model.Orders;
import com.store.userservice.model.Stock;
import com.store.userservice.model.User;
import com.store.userservice.repository.OrderRepository;
import com.store.userservice.repository.StockRepository;
@Service
@Transactional
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private StockService stockService;
	@Autowired
	private StockRepository stockRepository;
	public static double getTotalForStock(Stock stock, int quantity) {
		return quantity * stock.getPrice();
	}	
	public Orders placeOrder(String Address,User user, Integer stockId,int quantity) {
       
       Stock stock = stockService.getStockById(stockId);
       Orders newOrder = new Orders();
       newOrder.setOrderdate(LocalDate.now());
       newOrder.setUser(user);
       newOrder.setAddress(Address);
       newOrder.setStock(stock);
       newOrder.setQuantity(quantity);
       newOrder.setTotalPrice(getTotalForStock(stock, quantity));
       newOrder.setOrdereditem(stock.getName());
       newOrder.setTypeOfTransaction("COD");
        return orderRepository.save(newOrder);
	}
	public boolean updateStock(Integer stockId, int quantity) {
		boolean stocks=false;
		Stock stock = stockService.getStockById(stockId);
		if(stock.getAvailability()>=quantity) {
			stock.setAvailability(stock.getAvailability()-quantity);
			stockRepository.save(stock);
			stocks=true;
		}
		return stocks;
	}	
	public List<Orders> viewOrderList(User user) {
		return orderRepository.findAll();
	}
		 public Orders getOrderById(Integer orderId) {
		 		return orderRepository.findById(orderId).orElse(null);
		 	}
  
    }
