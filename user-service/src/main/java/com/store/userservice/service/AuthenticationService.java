package com.store.userservice.service;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.store.userservice.config.MessageStrings;
import com.store.userservice.exceptions.AuthenticationFailException;
import com.store.userservice.model.AuthenticationToken;
import com.store.userservice.model.User;
import com.store.userservice.repository.TokenRepository;
import com.store.userservice.utils.Helper;
@Transactional
@Service
public class AuthenticationService {

    @Autowired
    TokenRepository repository;

    public AuthenticationToken saveConfirmationToken(AuthenticationToken authenticationToken) {
        return repository.save(authenticationToken);
    }

    public AuthenticationToken getToken(User user) {
        return repository.findTokenByUser(user);
    }

    public User getUser(String token) {
        AuthenticationToken authenticationToken = repository.findTokenByToken(token);
        if (Helper.notNull(authenticationToken)) {
            if (Helper.notNull(authenticationToken.getUser())) {
                return authenticationToken.getUser();
            }
        }
        return null;
    }

    public void authenticate(String token) throws AuthenticationFailException {
        if (!Helper.notNull(token)) {
            throw new AuthenticationFailException(MessageStrings.AUTH_TOEKN_NOT_PRESENT);
        }
        if (!Helper.notNull(getUser(token))) {
            throw new AuthenticationFailException(MessageStrings.AUTH_TOEKN_NOT_VALID);
        }
    }
}
