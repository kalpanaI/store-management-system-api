package com.store.userservice.service;

import static com.store.userservice.config.MessageStrings.USER_CREATED;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.store.userservice.config.MessageStrings;
import com.store.userservice.dto.ResponseDto;
import com.store.userservice.dto.SignInDto;
import com.store.userservice.dto.SignInResponseDto;
import com.store.userservice.dto.SignupDto;
import com.store.userservice.enums.ResponseStatus;
import com.store.userservice.enums.Role;
import com.store.userservice.exceptions.AuthenticationFailException;
import com.store.userservice.exceptions.CustomException;
import com.store.userservice.model.AuthenticationToken;
import com.store.userservice.model.User;
import com.store.userservice.repository.UserRepository;
import com.store.userservice.utils.Helper;
@Service
@Transactional
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthenticationService authenticationService;
    
    Logger logger = LoggerFactory.getLogger(UserService.class);
 
    public ResponseDto signUp(SignupDto signupDto)  throws CustomException {
        
        if (Helper.notNull(userRepository.findByUsername(signupDto.getUsername()))) {
            
            throw new CustomException("User already exists");
        }
        String encryptedPassword = signupDto.getPassword();
        try {
            encryptedPassword = hashPassword(signupDto.getPassword());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            logger.error("hashing password failed {}", e.getMessage());
        }

        User user = new User(signupDto.getFirstName(), signupDto.getLastName(), signupDto.getEmail(),signupDto.getUsername(),signupDto.getMobileNumber(), Role.USER, encryptedPassword  );
        User createdUser;
        try {
            
            createdUser = userRepository.save(user);
            
            final AuthenticationToken authenticationToken = new AuthenticationToken(createdUser);
            
            authenticationService.saveConfirmationToken(authenticationToken);
            
            return new ResponseDto(ResponseStatus.SUCCESS.toString(), USER_CREATED);
        } catch (Exception e) {
            
            throw new CustomException(e.getMessage());
        }
    }

    public SignInResponseDto signIn(SignInDto signInDao) throws CustomException {
        
        User user = userRepository.findByUsername(signInDao.getUsername());
        if(!Helper.notNull(user)){
            throw  new AuthenticationFailException("user not present");
        }
        try {
            
            if (!user.getPassword().equals(hashPassword(signInDao.getPassword()))){
                
                throw  new AuthenticationFailException(MessageStrings.WRONG_PASSWORD);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            logger.error("hashing password failed {}", e.getMessage());
            throw new CustomException(e.getMessage());
        }

        AuthenticationToken token = authenticationService.getToken(user);

        if(!Helper.notNull(token)) {
            
            throw new CustomException("token not present");
        }

        return new SignInResponseDto ("success", token.getToken());
    }

    public String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        String myHash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();
        return myHash;
    }
 
   public String resetPassword(String username, String password, String confirmPassword) {
	   String encryptedPassword = confirmPassword;

	   User user = userRepository.findByUsername(username);
	   if (password.equals((confirmPassword))) {
		   try {
	           encryptedPassword = hashPassword(confirmPassword);
	       } catch (NoSuchAlgorithmException e) {
	           e.printStackTrace();
	           logger.error("hashing password failed {}", e.getMessage());
	       }
		   user.setPassword((encryptedPassword));
		   userRepository.save(user);
		   return "sucessfully updated Password";
		   
	   } else {
		   return "Password And confirm Password are not same";
	   }
   }
}

