package com.store.userservice.service;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.store.userservice.model.Stock;
import com.store.userservice.repository.StockRepository;
@Transactional
@Service
public class StockService {
	@Autowired
    private StockRepository stockRepository;

    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

	    public List<Stock> listStocks() {
	        List<Stock> stocks = stockRepository.findAll();
	        List<Stock> stockDaos = new ArrayList<>();
	        for(Stock stock : stocks) {
	        	Stock stockDao = getStockFromDao(stock);
	            stockDaos.add(stockDao);
	        }
	        return stockDaos;
	    }
	    public static Stock getStockFromDao(Stock stock) {
	   
	        return stock;
	    }
	    public List<Stock> getStockByName(String name) {
			return stockRepository.findByName(name);
		}
        public Stock getStockById(Integer stockId) {
    		return stockRepository.findById(stockId).orElse(null);
    	}
         
	}
