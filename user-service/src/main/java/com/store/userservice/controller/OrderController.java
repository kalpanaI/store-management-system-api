package com.store.userservice.controller;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.store.userservice.config.ApiResponse;
import com.store.userservice.exceptions.AuthenticationFailException;
import com.store.userservice.exceptions.OrderNotFoundException;
import com.store.userservice.model.Orders;
import com.store.userservice.model.User;
import com.store.userservice.service.AuthenticationService;
import com.store.userservice.service.OrderService;
@RestController
@RequestMapping("com.store/order")
public class OrderController {

	Logger logger = LoggerFactory.getLogger(OrderController.class);
	@Autowired
	private OrderService orderService;
	@Autowired
	private AuthenticationService authenticationService;

    @PostMapping("/add/")
    public ResponseEntity<ApiResponse> placeOrder(@RequestParam("token") String token,@RequestParam Integer stockId,int quantity, String Address)
            throws AuthenticationFailException { 
    	
		ResponseEntity<ApiResponse> responseEntity=null;
     
        User user = authenticationService.getUser(token);
        
        orderService.placeOrder(Address,user,stockId,quantity);
        if(orderService.updateStock(stockId, quantity)) {
        	
               	responseEntity=new ResponseEntity<ApiResponse>(new ApiResponse(true, "Order has been placed"), HttpStatus.CREATED);
        
        }else {
        	responseEntity=new ResponseEntity<ApiResponse>(new ApiResponse(false, "stock unavailable"), HttpStatus.NOT_FOUND);
        }
		return responseEntity;
    }
    @GetMapping("/getAllOrders")
	public ResponseEntity<List<Orders>> viewOrderList(@RequestParam("token") String token) throws AuthenticationFailException {
	
	User user = authenticationService.getUser(token);
	List<Orders> orderDtoList = orderService.viewOrderList(user);
	return new ResponseEntity<>(orderDtoList, HttpStatus.OK);
	}
	 
	@GetMapping("/getOrderBy{id}")
	public ResponseEntity<Object> getOrderById(@PathVariable("id") Integer id, @RequestParam("token") String token)
          
	throws AuthenticationFailException {
      
      try {
          Orders order = orderService.getOrderById(id);
          return new ResponseEntity<>(order,HttpStatus.OK);
      }
      catch (OrderNotFoundException e) {
          return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
      }
   }
  
}