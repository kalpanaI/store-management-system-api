package com.store.userservice.controller;
import java.security.NoSuchAlgorithmException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.userservice.dto.ResponseDto;
import com.store.userservice.dto.SignInDto;
import com.store.userservice.dto.SignInResponseDto;
import com.store.userservice.dto.SignupDto;
import com.store.userservice.exceptions.CustomException;
import com.store.userservice.header.HeaderGenerator;
import com.store.userservice.repository.UserRepository;
import com.store.userservice.service.AuthenticationService;
import com.store.userservice.service.UserService;

@RequestMapping("user")
@RestController
@Validated
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    UserService userService;
   
    HeaderGenerator headerGenerator=new HeaderGenerator();

    @PostMapping("/signup")
    public ResponseDto Signup(@Valid @RequestBody SignupDto signupDto) throws CustomException {
        return userService.signUp(signupDto);
    }

   
    @PostMapping("/signIn")
    public SignInResponseDto SignIn(@Valid @RequestBody SignInDto signInDto) throws CustomException {
        return userService.signIn(signInDto);
    }
    
    @PutMapping("/resetPassword/{username}/{password}/{confirmPassword}")
    public ResponseEntity<String> resetPassword(@Valid @PathVariable("username") String username,
    
    @PathVariable("password") String password, @PathVariable("confirmPassword") String confirmPassword) throws NoSuchAlgorithmException {
    String str = userService.resetPassword(username, password, confirmPassword);
    if (!str.isEmpty()) {
    return new ResponseEntity<String>(str, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
    }
    return new ResponseEntity<String>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
    }
    
}
