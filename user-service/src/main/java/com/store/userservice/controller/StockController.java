package com.store.userservice.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.store.userservice.exceptions.AuthenticationFailException;
import com.store.userservice.exceptions.StockNotFoundException;
import com.store.userservice.header.HeaderGenerator;
import com.store.userservice.model.Stock;
import com.store.userservice.service.AuthenticationService;
import com.store.userservice.service.StockService;
@RestController
@RequestMapping("/stock")
public class StockController {
    @Autowired 
    StockService stockService;
    @Autowired
    AuthenticationService authenticationService;
    
    HeaderGenerator headerGenerator=new HeaderGenerator();
    @GetMapping("/stockList")
    public ResponseEntity<List<Stock>> getStocks(@RequestParam("token") String token) throws AuthenticationFailException {
        
        List<Stock> body = stockService.listStocks();
        return new ResponseEntity<List<Stock>>(body, HttpStatus.OK);
    }
 
	@GetMapping("/stock/{name}")
	public ResponseEntity<List<Stock>> getStockByName(@RequestParam("name") String name) {
		List<Stock> stocks = stockService.getStockByName(name);
		if (!stocks.isEmpty()) {
			return new ResponseEntity<List<Stock>>(stocks, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<Stock>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
	@GetMapping("/getStockBy{id}")
	public ResponseEntity<Object> getStockById(@PathVariable("id") Integer stockId, @RequestParam("token") String token)
		throws AuthenticationFailException {
		try {
			Stock stocks = stockService.getStockById(stockId);
		return new ResponseEntity<>(stocks,HttpStatus.OK);
	}
		catch (StockNotFoundException e) {
		return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
	}
    }
	
}
