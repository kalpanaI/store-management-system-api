package com.store.adminservice.exceptions;

public class StockNotFoundException extends IllegalArgumentException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StockNotFoundException(String msg) {
        super(msg);
    }
}
