package com.store.adminservice.service;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.store.adminservice.model.Stock;
import com.store.adminservice.repository.StockRepository;
@Transactional
@Service
public class StockService {
	@Autowired
    private StockRepository stockRepository;
	 
	    public List<Stock> listStocks() {
	        List<Stock> stocks = stockRepository.findAll();
	        List<Stock> stockDaos = new ArrayList<>();
	        for(Stock stock : stocks) {
	        	Stock stockDao = getStockFromDao(stock);
	            stockDaos.add(stockDao);
	        }
	        return stockDaos;
	    }

	    public static Stock getStockFromDao(Stock stock) {
	        return stock;
	    }

	    public Stock addStock(Stock stock) {
	     return stockRepository.save(stock);
	    }

	    public Stock updateStock(Integer stockID, Stock stock) {
	        stock.setId(stockID);
	        return stockRepository.save(stock);
	    }
	    public Stock getStockById(Integer stockId) {
    		return stockRepository.findById(stockId).orElse(null);
    	}
	    public String deleteStock(Integer id) {
			stockRepository.deleteById(id);
			return "Stock Deleted successfully";
			
		}

		

}
