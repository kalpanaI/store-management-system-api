package com.store.adminservice.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.store.adminservice.model.Orders;
import com.store.adminservice.model.User;
import com.store.adminservice.repository.OrderRepository;
@Transactional
@Service
public class OrderService {
	@Autowired
	private OrderRepository orderRepository;
	
	public List<Orders> viewOrderList(User user) {
		return orderRepository.findAll();
	}
	 public Orders getOrderById(Integer orderId) {
 		return orderRepository.findById(orderId).orElse(null);
 	}
      
	public List<Orders> getAllOrdersById(Integer id) {
		return orderRepository.findAllOrdersByUserId(id);
	}
}