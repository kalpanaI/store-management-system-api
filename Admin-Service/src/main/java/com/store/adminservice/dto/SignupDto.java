package com.store.adminservice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hamcrest.Matcher;
import org.springframework.validation.annotation.Validated;

import com.store.adminservice.model.User;
import com.store.adminservice.validation.ValidEmail;
import com.store.adminservice.validation.ValidPassword;
@Validated
public class SignupDto {

 
	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your first name")
    private String firstName;
   
    
 
	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your last name")
    private String lastName;
    
    
 
	@ValidEmail(message = "*Please provide an email")
    @NotEmpty(message = "*Please provide an email")
    private String email;
    
 
    @Size(min=10,max=12)
    private String MobileNumber;

 
	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your name")
    private String username;
    

	@Size(min = 8, message = "*Your password must have at least 8 characters")
    @NotEmpty(message = "*Please provide your password")
	@ValidPassword
    private String password;
    
	public SignupDto(@Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your first name") String firstName,
			@Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your last name") String lastName,
			@NotEmpty(message = "*Please provide an email") String email, @Size(min = 10, max = 12) String mobileNumber,
			@Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your name") String username,
			@Size(min = 8, message = "*Your password must have at least 8 characters") @NotEmpty(message = "*Please provide your password") String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		MobileNumber = mobileNumber;
		this.username = username;
		this.password = password;
	}

	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String MobileNumber) {
		this.MobileNumber = MobileNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
