package com.store.adminservice.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.store.adminservice.config.ApiResponse;
import com.store.adminservice.exceptions.AuthenticationFailException;
import com.store.adminservice.exceptions.StockNotFoundException;
import com.store.adminservice.header.HeaderGenerator;
import com.store.adminservice.model.Stock;
import com.store.adminservice.service.AuthenticationService;
import com.store.adminservice.service.StockService;
@RestController
@RequestMapping("/stock")
public class StockController {
    @Autowired 
    StockService stockService;
  
    HeaderGenerator headerGenerator=new HeaderGenerator();
    
    @Autowired
	private AuthenticationService authenticationService;
    
    @Transactional
    @PostMapping("/addStock")
    public ResponseEntity<ApiResponse>addStock(@RequestParam("token") String token,@RequestBody Stock stock)throws AuthenticationFailException {
    	stockService.addStock(stock);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "Stock has been added"), HttpStatus.CREATED);
    }
    @GetMapping("/getAllStock")
    public ResponseEntity<List<Stock>> getStocks(@RequestParam("token") String token)throws AuthenticationFailException{
    	List<Stock> body = stockService.listStocks();
        return new ResponseEntity<List<Stock>>(body, HttpStatus.OK);
    }
    @PostMapping("/updateStock/{stockID}")
    public ResponseEntity<ApiResponse> updateStock(@RequestParam("token") String token,@PathVariable("stockID") Integer stockID, @RequestBody Stock stock)throws AuthenticationFailException {
    	stockService.updateStock(stockID, stock);
        return new ResponseEntity<ApiResponse> (new ApiResponse(true, "Stock has been updated"), HttpStatus.OK);
    }
    @DeleteMapping("/deleteStock/{stockId}")
	public ResponseEntity<String> deleteStockById(@RequestParam("token") String token,@RequestParam("id") Integer id)throws AuthenticationFailException{
 
    	String str = stockService.deleteStock(id);
		if (str != null) {
			return new ResponseEntity<String>(str, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
    @GetMapping("/getStockBy{id}")
   	public ResponseEntity<Object> getStockById(@PathVariable("id") Integer stockId, @RequestParam("token") String token)
   		throws AuthenticationFailException {
   		
   		try {
   			Stock stocks = stockService.getStockById(stockId);
   		return new ResponseEntity<>(stocks,HttpStatus.OK);
   	}
   		catch (StockNotFoundException e) {
   		return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
   	}
       }
}
