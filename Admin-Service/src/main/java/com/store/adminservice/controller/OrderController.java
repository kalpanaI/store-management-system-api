package com.store.adminservice.controller;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.store.adminservice.exceptions.AuthenticationFailException;
import com.store.adminservice.exceptions.OrderNotFoundException;
import com.store.adminservice.header.HeaderGenerator;
import com.store.adminservice.model.Orders;
import com.store.adminservice.model.User;
import com.store.adminservice.service.AuthenticationService;
import com.store.adminservice.service.OrderService;

@RestController
@RequestMapping("com.store/order")
public class OrderController {

	Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	private OrderService orderService;
	@Autowired
	private AuthenticationService authenticationService;
	
	HeaderGenerator headerGenerator=new HeaderGenerator();
	
	@GetMapping("/getAllOrders")
	public ResponseEntity<List<Orders>> viewOrderList(@RequestParam("token") String token) throws AuthenticationFailException {
		User user = authenticationService.getUser(token);
		List<Orders> orderDtoList = orderService.viewOrderList(user);
		return new ResponseEntity<>(orderDtoList, HttpStatus.OK);
	}
	@GetMapping("/getOrderBy{id}")
	public ResponseEntity<Object> getOrderById(@PathVariable("id") Integer id, @RequestParam("token") String token)
		throws AuthenticationFailException {
		try {
		Orders order = orderService.getOrderById(id);
		return new ResponseEntity<>(order,HttpStatus.OK);
	}
		catch (OrderNotFoundException e) {
		return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
	}
	}
	@GetMapping("/orders/{userId}")
	public ResponseEntity<List<Orders>> getAllOrdersByUserId(@RequestParam("userId") Integer userId) {
		List<Orders> orders = orderService.getAllOrdersById(userId);
		if (!orders.isEmpty()) {
		return new ResponseEntity<List<Orders>>(orders, headerGenerator.getHeadersForSuccessGetMethod(),
		HttpStatus.OK);
	}
		return new ResponseEntity<List<Orders>>(orders,headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
}