package com.store.adminservice.controller;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.store.adminservice.dto.ResponseDto;
import com.store.adminservice.dto.SignInDto;
import com.store.adminservice.dto.SignInResponseDto;
import com.store.adminservice.dto.SignupDto;
import com.store.adminservice.exceptions.AuthenticationFailException;
import com.store.adminservice.exceptions.CustomException;
import com.store.adminservice.header.HeaderGenerator;
import com.store.adminservice.model.User;
import com.store.adminservice.repository.UserRepository;
import com.store.adminservice.service.AuthenticationService;
import com.store.adminservice.service.UserService;

@RequestMapping("user")
@RestController
@Validated
public class UserController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthenticationService authenticationService;
 
    HeaderGenerator headerGenerator=new HeaderGenerator();
    @Autowired
    UserService userService;
    
    @GetMapping("/all")
    public List<User> findAllUser(@RequestParam("token") String token) throws AuthenticationFailException {
        return userRepository.findAll();
    }

    @PostMapping("/signUp")
    public ResponseDto Signup(@Valid @RequestBody SignupDto signupDto) throws CustomException {
        return userService.signUp(signupDto);
    }

    @PostMapping("/signIn")
    public SignInResponseDto SignIn(@Valid @RequestBody SignInDto signInDto) throws CustomException {
        return userService.signIn(signInDto);

     }

	@PutMapping("/resetPassword/{username}/{password}/{confirmPassword}")
    public ResponseEntity<String> resetPassword(@Valid @PathVariable("username") String username,
    
    @PathVariable("password") String password, @PathVariable("confirmPassword") String confirmPassword) throws NoSuchAlgorithmException {
    String str = userService.resetPassword(username, password, confirmPassword);
    if (!str.isEmpty()) {
    return new ResponseEntity<String>(str, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
    }
    return new ResponseEntity<String>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
    }
	
}
