package com.store.adminservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.store.adminservice.enums.Role;
import com.store.adminservice.validation.ValidEmail;
import com.store.adminservice.validation.ValidPassword;
@Entity
@Table(name = "users")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@Validated
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "first_name")
	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your first name")
    private String firstName;
   
    
    @Column(name = "last_name")
	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your last name")
    private String lastName;
    
    
    @Column(name = "email_id")
	@ValidEmail(message = "*Please provide an email")
    @NotEmpty(message = "*Please provide an email")
    private String email;
    
    @Column(name = "mobile_number")
    @Size(min=10,max=12)
    private String MobileNumber;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @Column(name = "username")
	@Size(min = 3, max = 15)
	@NotEmpty(message = "*Please provide your name")
    private String username;
    

	@Column(name = "password")
	@Size(min = 8, message = "*Your password must have at least 8 characters")
    @NotEmpty(message = "*Please provide your password")
	@ValidPassword
    private String password;
    

	public User(@NotEmpty(message = "*Please provide your name") String username,
			@Size(min = 5, message = "*Your password must have at least 5 characters") @NotEmpty (message = "*Please provide your password") String password) {
		super();
		this.username = username;
		this.password = password;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String MobileNumber) {
		this.MobileNumber = MobileNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	public User(String firstName, String lastName, String email,String username,String MobileNumber, Role role, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.MobileNumber=MobileNumber;
        this.username=username;
        this.role = role;
        this.password = password;
       
    }

    public User(Integer id,
			@Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your first name") String firstName,
			@Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your last name") String lastName,
			@NotEmpty(message = "*Please provide an email") String email, @Size(min = 10, max = 12) String mobileNumber,
			Role role, @Size(min = 3, max = 15) @NotEmpty(message = "*Please provide your name") String username,
			@Size(min = 8, message = "*Your password must have at least 8 characters") @NotEmpty(message = "*Please provide your password") String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		MobileNumber = mobileNumber;
		this.role = role;
		this.username = username;
		this.password = password;
	}

	public User() {
    }

}