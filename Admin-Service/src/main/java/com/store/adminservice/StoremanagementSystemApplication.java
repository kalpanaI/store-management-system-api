package com.store.adminservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@SpringBootApplication
public class StoreManagementSystemApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(StoreManagementSystemApplication.class, args);
		
	}
	@Bean
    public Docket stockApi() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.store.adminservice"))
                .paths(PathSelectors.any())
                .build();
    }
	
	private ApiInfo getApiInfo() {
        Contact contact = new Contact("Store.com", "http://Store.html", "contact.kalpanaI@gmail.com");
        return new ApiInfoBuilder()
                .title("Store Management System")
                .description("Admin Service for Store Management System")
                .version("1.0.0")
                .license("Apache 2.0")
                .contact(contact)
                .build();
    }
}
