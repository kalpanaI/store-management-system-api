package com.store.adminservice.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.store.adminservice.model.Orders;
@Repository
public interface OrderRepository extends JpaRepository<Orders, Integer>{

	boolean existsById(int orderid);

	Orders getOne(int orderid);
	public List<Orders> findAllOrdersByUserId(Integer id);
	
	
}
