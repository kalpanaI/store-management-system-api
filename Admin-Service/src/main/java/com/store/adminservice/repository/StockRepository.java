package com.store.adminservice.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.adminservice.model.Stock;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {

	

}
