package com.store.adminservice.enums;

public enum ResponseStatus
{
    SUCCESS,
    ERROR
}
