package com.store.adminservice;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.store.adminservice.model.Orders;
import com.store.adminservice.model.Stock;
import com.store.adminservice.model.User;
import com.store.adminservice.repository.OrderRepository;
import com.store.adminservice.service.OrderService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class OrderServiceTest {

	@MockBean
	OrderRepository orderRepository;
	@Autowired
	OrderService orderService;
	User user=new User();
	Stock stock=new Stock();
	Orders orders=new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock);

	@Test
	@Order(1)
	public void viewOrderListtest() {
			List<Orders> orders = orderRepository.findAll();
			User user=new User();
			orders.add(new Orders(1,2,"Bags",1200.00d,LocalDate.now(),"Nalgonda","COD",user,stock));
			when(orderRepository.findAll()).thenReturn(orders);			
			assertEquals(orders.size(),orderService.viewOrderList(user).size());
		}
	
    @Test
	@Order(2)
	public void testgetOrderById() {		
    	when(orderRepository.findById(1)).thenReturn(Optional.of(orders));
		assertTrue(orders.equals(orderService.getOrderById(1)));
	}

	@Test 
	@Order(3)
	public void testgetAllOrdersByuserId() {
		List<Orders> order=new ArrayList<Orders>();
		order.add(orders);
		when(orderRepository.findAllOrdersByUserId(1)).thenReturn(order);
		assertEquals(order,orderService.getAllOrdersById(1));
	}
}
