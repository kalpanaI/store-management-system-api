package com.store.adminservice;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.NoSuchAlgorithmException;

import org.junit.Before;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.test.util.ReflectionTestUtils;

import com.store.adminservice.controller.UserController;
import com.store.adminservice.dto.ResponseDto;
import com.store.adminservice.dto.SignInDto;
import com.store.adminservice.dto.SignInResponseDto;
import com.store.adminservice.dto.SignupDto;
import com.store.adminservice.enums.Role;
import com.store.adminservice.model.AuthenticationToken;
import com.store.adminservice.model.User;
import com.store.adminservice.repository.UserRepository;
import com.store.adminservice.service.AuthenticationService;
import com.store.adminservice.service.UserService;
@TestMethodOrder(OrderAnnotation.class)
@RunWith(MockitoJUnitRunner.class)
class UserServiceTest {

	@Mock
	private UserRepository userRepository=mock(UserRepository.class);
	
	@Mock
    SignupDto signupdto;
	@Mock
	UserController userController=mock(UserController.class);
	
	AuthenticationToken authenticationtoken=new AuthenticationToken();
	@Mock
	AuthenticationService authenticationService=mock(AuthenticationService.class);
	@InjectMocks
    UserService userService=new UserService();
	
	String token="89iujhghjnbbvcgh";
	User user=new User("kalpana","ithagoni","kalpanaithagoni@gmail.com","KalpanaI","99887788777",Role.ADMIN,"97DB8C538440CE84AC57FFD99A7A395E");
	
	
	@SuppressWarnings("deprecation")
	@Before public void init()
	{ 
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(userService, "userRepository", userRepository);
		ReflectionTestUtils.setField(userService, "authenticationService", authenticationService);
		}
	
	
	@Test
	@Order(1)
	public void testsignUp() throws NoSuchAlgorithmException {
		init();
		when(userRepository.findByUsername(Mockito.any())).thenReturn(null);
		when(authenticationService.saveConfirmationToken(Mockito.any())).thenReturn(authenticationtoken);
		SignupDto signupdto=new SignupDto("kalpana1","ithagoni","kalpanaithagoni@gmail.com","9988778877","Kalpana1","Kalpana@3");		
		signupdto.setPassword(token);
		ResponseDto responsedto=userService.signUp(signupdto);
		userService.hashPassword(user.getPassword());
		assertEquals(responsedto.getStatus(),"success");
		assertEquals("user created successfully",responsedto.getMessage());
		
		
	}
	


	@Test
	@Order(2)
	public void testsignIn() throws NoSuchAlgorithmException {
		init();
		authenticationtoken.setToken(token);
		when(userRepository.findByUsername(Mockito.any())).thenReturn(user);
		when(authenticationService.getToken(Mockito.anyObject())).thenReturn(authenticationtoken);	
		SignInDto signin=new SignInDto("Kalpana","Nalgonda@18");
	    signin.setPassword(token);
		SignInResponseDto responsedto=userService.signIn(signin);
		assertEquals("success",responsedto.getStatus());
		assertEquals(token,responsedto.getToken());	
	}

	@Test
	@Order(3)
	public void testresetpassword() throws NoSuchAlgorithmException {
		init();
		user.setPassword(token);
		when(userRepository.findByUsername(Mockito.any())).thenReturn(user);
		String responsedto=userService.resetPassword("Kalpana","Kalpana@9","Kalpana@9");
		assertEquals("sucessfully updated Password",responsedto);
		assertNotEquals("Password And confirm Password are not same",responsedto);
	}
}
