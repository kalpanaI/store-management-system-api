package com.store.adminservice;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.store.adminservice.model.Stock;
import com.store.adminservice.repository.StockRepository;
import com.store.adminservice.service.StockService;
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
class StockServiceTest {
	@MockBean
	StockRepository stockRepository;
	
	@Autowired
	StockService stockService;
	
	@Test
	@Order(1)
	public void testAddStock() {
		Stock stock=new Stock("Books",800.00,'A',20);
				when(stockRepository.save(stock)).thenReturn(stock);
				assertEquals(stock, stockService.addStock(stock));
	}		
	
	@Test
	@Order(2)
	public void testReadAllStock () {
		List<Stock> stocks =new ArrayList<Stock>();
		stocks.add(new Stock("Book",500.00,'A',20));
		stocks.add(new Stock("Bags",600.00,'A',10));
		when(stockRepository.findAll()).thenReturn(stocks);//Mocking
		assertEquals(2,stockService.listStocks().size());
	}	
	@Test
	@Order(3)
	public void testUpdateStock() {
		Stock stock=new Stock("Book",500.00,'A',20);
		Integer stockId=1;
		stock.setId(stockId);
		when(stockRepository.save(stock)).thenReturn(stock);
		assertEquals(stock,stockService.updateStock(stockId, stock));
	
	}
	@Test
	@Order(4)
	public void testgetStockById() {		
		Stock stock=new Stock("Book",500.00,'A',20);
    	when(stockRepository.findById(1)).thenReturn(Optional.of(stock));
		assertTrue(stock.equals(stockService.getStockById(1)));
	}
	@Test
	@Order(5)
	public void testDeleteStock () {
		stockService.deleteStock(1);
		verify(stockRepository,times(1)).deleteById(1); //Mocking	
		assertEquals("Stock Deleted successfully",stockService.deleteStock(1));
	}
}
