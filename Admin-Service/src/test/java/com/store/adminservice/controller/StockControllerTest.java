package com.store.adminservice.controller;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.store.adminservice.config.ApiResponse;
import com.store.adminservice.enums.Role;
import com.store.adminservice.model.Stock;
import com.store.adminservice.model.User;
import com.store.adminservice.service.AuthenticationService;
import com.store.adminservice.service.StockService;
@TestMethodOrder(OrderAnnotation.class)
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class StockControllerTest {
	@Mock
	AuthenticationService authenticationService;
	@Mock
	StockService stockService;
	@InjectMocks
	StockController stockController;
	User user = new User(1,"kalpana","ithagoni","kalpanaithagoni@gmail.com","99887788777",Role.ADMIN,"Kalpana15","Kalpana@3");
	List<Stock> stocks;
	Stock stock;
	String token;
	
	@Test
	@Order(1)
	public void testaddStock() {
		stock=new Stock(1,"Books",800.00,'A',20);
		when(stockService.addStock(stock)).thenReturn(stock);
		ResponseEntity<ApiResponse> res=stockController.addStock(token, stock);
		assertEquals(HttpStatus.CREATED,res.getStatusCode());
		assertEquals("Stock has been added",res.getBody().getMessage());
		assertEquals(true,res.getBody().isSuccess());
		assertEquals(LocalDateTime.now().toString(),res.getBody().getTimestamp());
	}

	@Test
	@Order(2)
	public void testgetStocks() {
		stocks=new ArrayList<Stock>();
		stocks.add(new Stock(1,"Books",800.00,'A',20));
		stocks.add(new Stock(2,"Bags",1200.00,'A',10));
		when(authenticationService.getUser(token)).thenReturn(user); //mocking
		when(stockService.listStocks()).thenReturn(stocks); //mocking
		ResponseEntity<List<Stock>> res=stockController.getStocks(token);
		assertEquals(HttpStatus.OK,res.getStatusCode());
		assertEquals(2,res.getBody().size());
	}
	@Test
	@Order(3)
	public void testUpdateStock() {
		stock=new Stock(1,"Books",800.00,'A',20);
		Integer stockId=1;
		when(stockService.getStockById(stockId)).thenReturn(stock);
		when(stockService.updateStock(stockId, stock)).thenReturn(stock);
		ResponseEntity<ApiResponse> res=stockController.updateStock(token, null, stock);
		assertEquals(HttpStatus.OK,res.getStatusCode());
		assertEquals("Stock has been updated",res.getBody().getMessage());
		assertEquals(true,res.getBody().isSuccess());
		assertEquals(LocalDateTime.now().toString(),res.getBody().getTimestamp());
	}
	@Test
	@Order(4)
	public void testDeleteStock() {
		stock=new Stock(1,"Books",800.00,'A',20);
		when(stockService.getStockById(1)).thenReturn(stock);
		ResponseEntity<String> res=stockController.deleteStockById(token, 1);
		assertNotEquals(HttpStatus.OK,res.getStatusCode());
		

	}
	@Test
	@Order(5)
	public void testgetStockById() {
		stock=new Stock(1,"Books",800.00,'A',30);
		when(stockService.getStockById(1)).thenReturn(stock);	//Mocking
		ResponseEntity<Object> res=stockController.getStockById(1, token);
		assertEquals(HttpStatus.OK,res.getStatusCode());
	}
}
