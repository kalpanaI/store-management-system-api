package com.store.adminservice.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.store.adminservice.model.Orders;
import com.store.adminservice.model.Stock;
import com.store.adminservice.model.User;
import com.store.adminservice.service.AuthenticationService;
import com.store.adminservice.service.OrderService;
import com.store.adminservice.controller.OrderControllerTest;
import com.store.adminservice.enums.Role;
@TestMethodOrder(OrderAnnotation.class)
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class OrderControllerTest {
	
	@Mock
	OrderService orderService;
	
	@Mock
	AuthenticationService authenticationService;
	
	@InjectMocks
	OrderController ordercontroller;
	Stock stock=new Stock();
	User user = new User(1,"kalpana","ithagoni","kalpanaithagoni@gmail.com","99887788777",Role.ADMIN,"Kalpana15","Kalpana@3");
	Orders order = new Orders(1,2,"Nalgonda",1200.00d,LocalDate.now(),"Bags","COD",user,stock);
	String token = null;
	List<Orders> orders;
	Orders Order;
	
	@Test
	@Order(1)
	public void testviewOrderList(){
		orders=new ArrayList<Orders>();
		orders.add(new Orders(1,2,"Nalgonda",1200.00d,LocalDate.now(),"Bags","COD",user,stock));
		orders.add(new Orders(2,5,"Hyderabad",1500.00d,LocalDate.now(),"Books","COD",user,stock));
		when(authenticationService.getUser(token)).thenReturn(user); //Mocking
		when(orderService.viewOrderList(user)).thenReturn(orders); //Mocking
		ResponseEntity<List<Orders>> res=ordercontroller.viewOrderList(token);
		assertNotEquals(HttpStatus.FOUND,res.getStatusCode());
		assertEquals(2,res.getBody().size());

 }
	@Test
	@Order(2)
	public void testgetOrderById() {
		Order=new Orders(1,2,"Nalgonda",1200.00d,LocalDate.now(),"Bags","COD",user,stock);
		
		when(orderService.getOrderById(1)).thenReturn(Order);	//Mocking
		ResponseEntity<Object> res=ordercontroller.getOrderById(1, token);
		assertNotEquals(HttpStatus.FOUND,res.getStatusCode());
		assertNotEquals(1,res.getBody().getClass());
	}
	
	@Test
	@Order(3)
	public void testgetAllOrdersByUserId() {
		List<Orders> orders = new ArrayList<Orders>();
		orders.add(order);
		Integer userId=0;
		when(orderService.getAllOrdersById(userId)).thenReturn(orders); //Mocking
		ResponseEntity<List<Orders>> res = ordercontroller.getAllOrdersByUserId(userId);
		assertEquals(HttpStatus.OK, res.getStatusCode());
		assertEquals(orders, res.getBody());
	}

}



